// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  apiKey: '', // Add your api key here (you must create an account on https://opendata.lillemetropole.fr/signup/)
  apiUrlBasis: 'https://opendata.lillemetropole.fr/api/v2/catalog/datasets/evenements-publics-openagenda/',
  exports: 'exports/json',
  globalParameters: '?where=date_start%3E%3Dnow()&sort=-date_start',
  apiUrlEnd: `&apikey=`,
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
