import { Injectable } from '@angular/core';
import { EventModel } from '../models/event.model';
import { SearchFields } from '../models/searchFields.model';

/**
 * The SearchFilterService applies research parameters/filters to the displayed events list
 */
@Injectable({
  providedIn: 'root'
})
export class SearchFilterService {

  constructor() { }

  /**
   * Gets an array of all events from eventsList with city attribute corresponding to the filter.
   * @param eventsList Array of EventModel objects to filter
   * @param city Filter parameter : city where the event takes place
   */
  applyCityFilter(eventsList: EventModel[], city: String): EventModel[] {
    return eventsList.filter(
      (ev: EventModel) => {
        if(ev.city && ev.city!=='') {
          return ev.city.toLowerCase() === city.toLowerCase();
        } else {
          return false;
        }
      }
    )
  }

  /**
   * Gets an array of all events from eventsList which will end after the filter.
   * @param eventsList Array of EventModel objects to filter
   * @param dateBegin Filter parameter : begin of the research period
   */
  applyDateBeginFilter(eventsList: EventModel[], dateBegin: Date): EventModel[] {
    return eventsList.filter(
      (ev: EventModel) => {
        if (ev.dateEnd) {
          return ev.dateEnd >= new Date(dateBegin);
        } else {
          return false;
        }
      }
    )
  }

  /**
   * Gets an array of all events from eventsList which will begin before the filter.
   * @param eventsList Array of EventModel objects to filter
   * @param dateEnd Filter parameter : end of the research period
   */
  applyDateEndFilter(eventsList: EventModel[], dateEnd: Date): EventModel[] {
    return eventsList.filter(
      (ev: EventModel) => {
        if (ev.dateStart) {
          return ev.dateStart <= new Date(dateEnd);
        } else {
          return false;
        }
      }
    )
  }

  /**
   * Gets an array of all events from eventsList which title, description or freeText contains the keyword parameter.
   * @param eventsList Array of EventModel objects to filter
   * @param keyword Filter parameter
   */
  applyKeywordFilter(eventsList: EventModel[], keyword: string): EventModel[] {
    return eventsList.filter(
      (ev: EventModel) => {
        if (ev.getTextContent()) {
          return ev.getTextContent().toLowerCase().includes(keyword.toLowerCase());
        } else {
          return false;
        }
      }
    )
  }

  /**
   * Apply all existing filters to the eventsList.
   * @param eventsList Array of EventModel objects to filter
   * @param searchFields SearchFields object containing all filter parameters
   */
  applyFilters(eventsList: Array<EventModel>, searchFields: SearchFields): Array<EventModel> {
    let filterResults: Array<EventModel> = eventsList;
    if (searchFields.city && searchFields.city !== '') {
      filterResults = this.applyCityFilter(filterResults, searchFields.city);
    }
    if (searchFields.periodBegin) {
      filterResults = this.applyDateBeginFilter(filterResults, searchFields.periodBegin);
    }
    if (searchFields.periodEnd) {
      filterResults = this.applyDateEndFilter(filterResults, searchFields.periodEnd);
    }
    if (searchFields.keyword && searchFields.keyword !== '') {    
      filterResults = this.applyKeywordFilter(filterResults, searchFields.keyword);
    }
    return filterResults;
  }
}
