import { Pipe, PipeTransform } from '@angular/core';
import { EventModel } from './models/event.model';

@Pipe({
  name: 'sortResult'
})
export class SortResultPipe implements PipeTransform {

  transform(value: EventModel[], ...args: any[]): any {
    return value.sort((a:EventModel, b:EventModel) => {
      return a.dateStart < b.dateStart ? -1 : a.dateStart > b.dateStart ? 1 : 0;
    });
  }

  
}
