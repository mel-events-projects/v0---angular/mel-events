import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, CheckboxControlValueAccessor } from '@angular/forms';
import { SearchFilterService } from 'src/app/services/search-filter.service';
import { SearchFields } from 'src/app/models/searchFields.model';

/**
 * SearchFormComponent displays and gets results from a form with research parameters
 */
@Component({
  selector: 'cdc-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  private searchForm: FormGroup;
  private keywordCtrl: FormControl;
  private cityCtrl: FormControl;
  private periodBeginCtrl: FormControl;
  private periodEndCtrl: FormControl;
  
  /**
   * Controls if a date is after the 1st day of this month.
   */
  static isAfterPeriodMin = (control: FormControl) => {
    const today = new Date();
    const periodMin: Date = new Date(today.getFullYear(), today.getMonth(), 1);
    const dateToCheck = new Date(control.value);
    return (!control.value || dateToCheck >= periodMin) ? null : { tooEarly: true, minDate: periodMin};
  }

  /**
   * Controls if a date is before next month next year.
   */
  static isBeforePeriodMax = (control: FormControl) => {
    const today = new Date();
    const periodMax: Date = new Date(today.getFullYear() + 1, today.getMonth() + 1, 1);
    const dateToCheck = new Date(control.value);
    return (!control.value || dateToCheck <= periodMax) ? null : { tooLate: true, maxDate: periodMax};
  }

  @Output() research = new EventEmitter<SearchFields>();
  @Output() cancel = new EventEmitter();

  constructor(private fb: FormBuilder, private searchService: SearchFilterService) {
    this.keywordCtrl = fb.control('', [Validators.minLength(3), Validators.pattern(`^([a-zA-Z]+[-' ]?[a-zA-Z])+$`)]);
    this.cityCtrl = fb.control('', [Validators.minLength(2), Validators.pattern(`^([a-zA-Z]+[-' ]?[a-zA-Z])+$`)]);
    this.periodBeginCtrl = fb.control('', [SearchFormComponent.isAfterPeriodMin, SearchFormComponent.isBeforePeriodMax]);
    this.periodEndCtrl = fb.control('', [SearchFormComponent.isAfterPeriodMin, SearchFormComponent.isBeforePeriodMax]);
    this.searchForm = fb.group({
      keyword: this.keywordCtrl,
      city: this.cityCtrl,
      periodBegin: this.periodBeginCtrl,
      periodEnd: this.periodEndCtrl
    })
   }

  ngOnInit() {
  }

  /**
   * Resets all input fields values and emit a cancel event.
   */
  reset() {
    this.keywordCtrl.setValue('');
    this.cityCtrl.setValue('');
    this.periodBeginCtrl.setValue('');
    this.periodEndCtrl.setValue('');
    this.cancel.emit();
  }

  /**
   * Instantiates a SearchFields object with research criteria and emit a research event.
   */
  search() {
    let searchFields = new SearchFields(this.searchForm.value.keyword, this.searchForm.value.city, this.searchForm.value.periodBegin, this.searchForm.value.periodEnd);
    this.research.emit(searchFields);
  }
}
