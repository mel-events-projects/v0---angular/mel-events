/**
 * This class is a model in order to display the timetable slots of each event in the application.
 */
export class TimetableSlot {
    begin: Date;
    end: Date;
}